(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Bee.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'ed792RwjjlAGoiljeXul0s+', 'Bee', __filename);
// script/Bee.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Bee = /** @class */ (function (_super) {
    __extends(Bee, _super);
    function Bee() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.vertical = false;
        _this.moveDistance = 100;
        _this.anim = null;
        return _this;
    }
    Bee.prototype.onLoad = function () {
        this.anim = this.node.getComponent(cc.Animation);
    };
    Bee.prototype.start = function () {
        this.anim.play("bee");
        //TODO:add bee's se
        var action;
        if (this.vertical) {
            action = cc.repeatForever(cc.sequence(cc.moveBy(2, 0, this.moveDistance), cc.delayTime(1), cc.moveBy(2, 0, -this.moveDistance), cc.delayTime(1)));
        }
        else {
            action = cc.repeatForever(cc.sequence(cc.moveBy(2, -this.moveDistance, 0), cc.delayTime(1), cc.flipX(true), cc.moveBy(2, this.moveDistance, 0), cc.delayTime(1), cc.flipX(false)));
        }
        this.node.runAction(action);
    };
    Bee.prototype.onBeginContact = function (contact, self, other) {
        if (other.tag == 9) {
            this.anim.play("hurt");
            console.log('bee die');
            this.scheduleOnce(function () {
                this.node.destroy();
            }, 0.6);
        }
    };
    __decorate([
        property
    ], Bee.prototype, "vertical", void 0);
    __decorate([
        property
    ], Bee.prototype, "moveDistance", void 0);
    Bee = __decorate([
        ccclass
    ], Bee);
    return Bee;
}(cc.Component));
exports.default = Bee;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Bee.js.map
        