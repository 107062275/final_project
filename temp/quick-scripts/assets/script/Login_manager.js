(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Login_manager.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '80664Hpqn9GyITBcnE2VARv', 'Login_manager', __filename);
// script/Login_manager.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Login_manager = /** @class */ (function (_super) {
    __extends(Login_manager, _super);
    function Login_manager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.email = null;
        _this.password = null;
        _this.se_click = null;
        _this.total_manager = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Login_manager.prototype.start = function () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
    };
    Login_manager.prototype.login = function (message) {
        var _this = this;
        cc.audioEngine.playEffect(this.se_click, false);
        var auth = firebase.auth();
        var log_in_email_txt = this.email.string;
        var log_in_password_txt = this.password.string;
        var account = this.email_to_account(log_in_email_txt);
        var promise = auth.signInWithEmailAndPassword(log_in_email_txt, log_in_password_txt);
        promise.then(function (s) {
            alert("Wellcome back! " + account + " !");
            document.body.style.cursor = "auto";
            cc.director.loadScene("menu");
        })
            .catch(function (e) { alert(e); log_in_email_txt = ""; log_in_password_txt = ""; });
        var postsRef = firebase.database().ref("account/" + account);
        this.total_manager.account = account;
        postsRef.once('value', function (data) {
            _this.total_manager.fruit = data.val().fruit;
            _this.total_manager.skin = data.val().skin;
            _this.total_manager.current_skin = data.val().current_skin;
            _this.total_manager.stage1_fruit = data.val().stage1_fruit;
            _this.total_manager.stage2_fruit = data.val().stage2_fruit;
            _this.total_manager.stage3_fruit = data.val().stage3_fruit;
        });
    };
    Login_manager.prototype.signup = function (message) {
        cc.audioEngine.playEffect(this.se_click, false);
        var auth = firebase.auth();
        var sign_up_email_txt = this.email.string;
        var sign_up_password_txt = this.password.string;
        var account_txt = this.email_to_account(sign_up_email_txt);
        var postsRef = firebase.database().ref('account/' + account_txt);
        postsRef.once('value').then(function (snapshot) {
            var data = snapshot.val();
            var promise = auth.createUserWithEmailAndPassword(sign_up_email_txt, sign_up_password_txt);
            promise.then(function (s) {
                alert("success");
                var postsRef = firebase.database().ref("account/" + account_txt);
                postsRef.set({
                    email: sign_up_email_txt,
                    //skin : data of which skin the player have (binary code method)
                    //     binary code method : if got #1 , #5 , #8 skin , then skin = 2^(1-1) + 2^(5-1) + 2^(8-1) = 145
                    //current_skin : the number of current skin that the player choose;
                    //stage_fruit : the number of fruit that player eaten
                    skin: 1,
                    fruit: 0,
                    current_skin: 1,
                    stage1_fruit: 0,
                    stage2_fruit: 0,
                    stage3_fruit: 0
                });
            }, function (e) { alert(e); });
        });
    };
    Login_manager.prototype.email_to_account = function (email) {
        var account_arr = [];
        for (var _i = 0, email_1 = email; _i < email_1.length; _i++) {
            var letter = email_1[_i];
            if (letter != "@")
                account_arr.push(letter);
            else
                break;
        }
        var account = account_arr.join("");
        console.log(account);
        return account;
    };
    Login_manager.prototype.auto_login = function (message) {
        var _this = this;
        cc.audioEngine.playEffect(this.se_click, false);
        var auth = firebase.auth();
        var log_in_email_txt = "123@123.com";
        var log_in_password_txt = "123123";
        var account = this.email_to_account(log_in_email_txt);
        var promise = auth.signInWithEmailAndPassword(log_in_email_txt, log_in_password_txt);
        promise.then(function (s) {
            alert("Wellcome back! " + account + " !");
            document.body.style.cursor = "auto";
            cc.director.loadScene("menu");
        })
            .catch(function (e) { alert(e); log_in_email_txt = ""; log_in_password_txt = ""; });
        var postsRef = firebase.database().ref("account/" + account);
        this.total_manager.account = account;
        postsRef.once('value', function (data) {
            _this.total_manager.fruit = data.val().fruit;
            _this.total_manager.skin = data.val().skin;
            _this.total_manager.current_skin = data.val().current_skin;
            _this.total_manager.stage1_fruit = data.val().stage1_fruit;
            _this.total_manager.stage2_fruit = data.val().stage2_fruit;
            _this.total_manager.stage3_fruit = data.val().stage3_fruit;
        });
    };
    __decorate([
        property({ type: cc.EditBox })
    ], Login_manager.prototype, "email", void 0);
    __decorate([
        property({ type: cc.EditBox })
    ], Login_manager.prototype, "password", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Login_manager.prototype, "se_click", void 0);
    Login_manager = __decorate([
        ccclass
    ], Login_manager);
    return Login_manager;
}(cc.Component));
exports.default = Login_manager;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Login_manager.js.map
        