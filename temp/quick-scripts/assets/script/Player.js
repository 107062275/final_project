(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Player.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '2feddn0ENFHBqfQcaZYf1Dv', 'Player', __filename);
// script/Player.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // the ratio of getting bigger when player eat a cell
        _this.bigger_rate = 1.1;
        // the ratio of camera zooming out when player eat a cell
        _this.camera_zoom_out_rate = 0.95;
        _this.jump_force = 500;
        _this.clone_cell = null;
        _this.jumpable_angle = 60;
        _this.bgm_stage1 = null;
        _this.se_jump = null; //se = sound effect
        _this.se_eatCell = null;
        _this.se_eatFruit = null;
        _this.se_hurt = null;
        _this.se_die = null;
        _this.skin1 = null;
        _this.skin2 = null;
        _this.skin3 = null;
        _this.skin4 = null;
        _this.skin5 = null;
        _this.skin6 = null;
        _this.skin7 = null;
        _this.isOnGround = false;
        _this.isJump = false;
        _this.account = null;
        _this.game_manager = null;
        _this.total_manager = null;
        _this.triScatter = false;
        _this.isScatter = false;
        _this.triGather = false;
        _this.canGather = false;
        _this.is_Udi = false;
        _this.canOpen = true;
        _this.eatenNum = 0;
        _this.scatterNum = 0;
        _this.cloneCellNum = 0;
        _this.cloneCellList = [0];
        _this.anim = null;
        _this.bodyAnim = null;
        _this.face_Node = null;
        _this.bgm_stage1_state = null;
        _this.isHurt = false;
        _this.abletbHurt = true;
        _this.hurtTime = 2;
        _this.isDead = false;
        _this.rebornPos = null;
        _this.isReborn = false;
        _this.rebornTime = 10;
        return _this;
    }
    Player.prototype.onLoad = function () {
        this.anim = this.node.getChildByName("face").getComponent(cc.Animation);
        this.bodyAnim = this.node.getComponent(cc.Animation);
    };
    Player.prototype.start = function () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
        this.game_manager = cc.find("Game_manager").getComponent("Game_manager");
        this.account = this.total_manager.account;
        //this.eatenNum = this.total_manager.cell;
        this.eatenNum = 0;
        this.camera = cc.find("Canvas/Main Camera").getComponent(cc.Camera);
        this.camera_Node = cc.find("Canvas/Main Camera");
        this.camera_script = this.camera_Node.getComponent("Camera_control");
        this.face_Node = this.node.getChildByName("face");
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.bgm_stage1_state = cc.audioEngine.playMusic(this.bgm_stage1, true);
        this.rebornPos = this.node.position; //n      
        this.init();
    };
    Player.prototype.init = function () {
        for (var i = 0; i < this.eatenNum; i++) {
            this.node.scaleX *= this.bigger_rate;
            this.node.scaleY *= this.bigger_rate;
            this.camera.zoomRatio *= this.camera_zoom_out_rate;
        }
        // current_skin is the number of skin that player choose in the store
        var current_skin = this.total_manager.current_skin;
        cc.log("used skin " + current_skin);
        // TODO : init player sprite to the skin
        //        note : the order of skin is the order of items in (scence)"store"/"Store_manager"/"Items"
        switch (current_skin) {
            case 1:
                this.node.getComponent(cc.Sprite).spriteFrame = this.skin1;
                break;
            case 2:
                this.node.getComponent(cc.Sprite).spriteFrame = this.skin2;
                break;
            case 3:
                this.node.getComponent(cc.Sprite).spriteFrame = this.skin3;
                break;
            case 4:
                this.node.getComponent(cc.Sprite).spriteFrame = this.skin4;
                break;
            case 5:
                this.node.getComponent(cc.Sprite).spriteFrame = this.skin5;
                break;
            case 6:
                this.node.getComponent(cc.Sprite).spriteFrame = this.skin6;
                break;
            case 7:
                this.node.getComponent(cc.Sprite).spriteFrame = this.skin7;
                break;
        }
        // init UI fruit label
        this.game_manager.update_UI_fruit_lable(this.total_manager.fruit);
    };
    Player.prototype.update = function (dt) {
        this.move();
        this.play_animate();
    };
    Player.prototype.move = function () {
        this.deadCtrl();
        if (this.scatterNum == 0) {
            this.isScatter = false;
        }
        else {
            this.isScatter = true;
        }
        if (this.isOnGround && this.isJump) {
            this.jump();
        }
        if (this.triScatter && this.eatenNum > 0) {
            this.scatter();
        }
        if (this.triGather) {
            console.log("scatterNum: " + this.scatterNum);
        }
        if (this.triGather && this.isScatter) {
            this.gather();
        }
        if (this.isHurt) {
            this.isHurt = false;
            this.abletbHurt = false;
            this.scheduleOnce(function () {
                console.log("able to be hurt again");
                this.abletbHurt = true;
            }, this.hurtTime);
        }
    };
    Player.prototype.play_animate = function () {
        //jump animation in jump()
        if (this.isDead && !this.bodyAnim.getAnimationState("die").isPlaying) {
            this.anim.play("dieFace");
            this.bodyAnim.play("die");
        }
        else if (this.anim.getAnimationState("eatFace").isPlaying || this.anim.getAnimationState("splitFace").isPlaying || this.anim.getAnimationState("hurtFace").isPlaying) {
            return;
        }
        else if (this.isOnGround && !this.anim.getAnimationState("idleFace").isPlaying) {
            this.anim.play("idleFace"); //blinking when it's on the floor
        }
        else if (!this.isOnGround && !this.anim.getAnimationState("jumpFace").isPlaying) {
            this.anim.play("fallFace"); //shock face while falling
        }
    };
    // tag 0 : player
    // tag 1 : wall
    // tag 2 : player's cell
    // tag 3 : clone cell
    // tag 4 : fruit
    // tag 5 : enemy
    // tag 6 : 传送门
    // tag 7 : 机关
    // tag 8 : 机关
    // tag 9 : 機關的兩道門（在Bee.ts中使用）
    // tag 10 : 机关
    // tag 11 : finish point
    // tag 12 & 13 : stage3 传送门
    Player.prototype.onBeginContact = function (contact, self, other) {
        var _this = this;
        if (this.isDead)
            return; // if dead then do nothing
        if (other.tag == 1) {
            var WorldManifold = contact.getWorldManifold().normal;
            cc.log("touch : " + WorldManifold);
            // if touch the ground which its angle compare to camera's horizonto is smaller than this.jumpable_angle
            if (this.on_jumpable_floor(WorldManifold)) {
                this.isOnGround = true;
            }
            else {
                this.isOnGround = false;
            }
        }
        else if (other.tag == 2) { //eat a cell and be bigger, also zoom out camera
            if (!this.anim.getAnimationState("eatFace").isPlaying) {
                this.anim.play("eatFace");
            }
            other.node.active = false;
            cc.audioEngine.playEffect(this.se_eatCell, false);
            // adjust UI apprearance with some ratio math
            this.game_manager.adjust_UI_apprearance(this.camera_zoom_out_rate);
            // adjust player size & camera size
            self.node.scaleX *= this.bigger_rate;
            self.node.scaleY *= this.bigger_rate;
            //this.getComponent(cc.PhysicsCircleCollider).radius *= Math.sqrt(this.bigger_rate);
            this.camera.zoomRatio *= this.camera_zoom_out_rate;
            this.eatenNum++;
            console.log("eaten: " + this.eatenNum);
            // update UI cell label
            this.game_manager.update_UI_cell_lable(this.eatenNum);
            // TODO? : zoom out smoothly(animation? action?)
        }
        else if (other.tag == 3) {
        }
        else if (other.tag == 4) { // eat a fruit
            cc.audioEngine.playEffect(this.se_eatFruit, false);
            this.total_manager.fruit += 1;
            other.node.active = false;
            this.total_manager.update_to_firebase("fruit", this.total_manager.fruit);
            if (this.total_manager.stage == 1) {
                this.total_manager.stage1_fruit += Math.pow(2, other.node.getComponent("Fruit").number - 1);
                this.total_manager.update_to_firebase("stage1_fruit", this.total_manager.stage1_fruit);
            }
            else if (this.total_manager.stage == 2) {
                this.total_manager.stage2_fruit += Math.pow(2, other.node.getComponent("Fruit").number - 1);
                this.total_manager.update_to_firebase("stage2_fruit", this.total_manager.stage2_fruit);
            }
            else if (this.total_manager.stage == 3) {
                this.total_manager.stage3_fruit += Math.pow(2, other.node.getComponent("Fruit").number - 1);
                this.total_manager.update_to_firebase("stage3_fruit", this.total_manager.stage3_fruit);
            }
            // update UI fruit label
            this.game_manager.update_UI_fruit_lable(this.total_manager.fruit);
        }
        else if (other.tag == 5) { // touch enemy
            if (this.is_Udi)
                return;
            cc.audioEngine.playEffect(this.se_hurt, false);
            console.log("touch enemy!");
            console.log(this.isScatter);
            console.log(this.isHurt);
            this.anim.play("hurtFace");
            this.bodyAnim.play("hurt");
            this.is_Udi = true;
            this.scheduleOnce(function () {
                this.anim.stop();
                this.bodyAnim.stop();
                this.node.opacity = 255;
                this.is_Udi = false;
            }, 1.5);
            if (this.eatenNum != 0 && this.abletbHurt) { //have extra life but didn't scatter
                this.isHurt = true;
                this.anim.play("hurt");
                this.eatenNum--;
                console.log("eaten: " + this.eatenNum);
                this.node.scaleX /= this.bigger_rate;
                this.node.scaleY /= this.bigger_rate;
                //this.getComponent(cc.PhysicsCircleCollider).radius /= Math.sqrt(this.bigger_rate);
                this.camera.zoomRatio /= this.camera_zoom_out_rate;
                this.anim.play("hurtFace");
                // update UI cell label
                this.game_manager.update_UI_cell_lable(this.eatenNum);
                // adjust UI apprearance with some ratio math
                this.game_manager.adjust_UI_apprearance(1 / this.camera_zoom_out_rate);
            }
            else if (this.eatenNum == 0) { //no extra life and going to die
                this.anim.play("hurtFace");
                cc.audioEngine.playEffect(this.se_die, false);
                this.isDead = true;
            }
        }
        else if (other.tag == 6 || other.tag == 12 || other.tag == 13) {
            if (other.tag == 6) {
                var action = cc.sequence(cc.fadeOut(1), cc.jumpTo(0.1, 3800, -680, 1000, 1), cc.fadeIn(1));
                this.node.runAction(action);
            }
            else if (other.tag == 12) {
                var action = cc.sequence(cc.fadeOut(1), cc.jumpTo(0.1, 4410, -560, 1000, 1), cc.fadeIn(1));
                this.node.runAction(action);
            }
            else if (other.tag == 13) {
                var action = cc.sequence(cc.fadeOut(1), cc.jumpTo(0.1, 5860, 538, 1000, 1), cc.fadeIn(1));
                this.node.runAction(action);
            }
        }
        else if (other.tag == 7 || other.tag == 8 || other.tag == 10) {
            this.isOnGround = true;
            if (this.canOpen == true) {
                this.canOpen = false;
                var actionl = cc.sequence(cc.moveBy(1, 70, 0), cc.delayTime(0.2), cc.moveBy(1, -70, 0));
                var action2 = cc.sequence(cc.moveBy(1, -70, 0), cc.delayTime(0.2), cc.moveBy(1, 70, 0));
                if (other.tag == 7) {
                    cc.find("jiguan/left").runAction(actionl);
                    cc.find("jiguan/right").runAction(action2);
                }
                else if (other.tag == 8) {
                    cc.find("jiguan/left_1").runAction(actionl);
                    cc.find("jiguan/right_1").runAction(action2);
                }
                else if (other.tag == 10) {
                    cc.find("jiguan/left_2").runAction(actionl);
                    cc.find("jiguan/right_2").runAction(action2);
                }
                this.scheduleOnce(function () {
                    this.canOpen = true;
                }, 2.2);
            }
        }
        else if (other.tag == 11) {
            // ADD ANY WIN AUDIO OR ANIMATION HERE
            this.schedule(function () { _this.game_manager.go_to_win(); }, 3);
        }
    };
    Player.prototype.onPostSolve = function (contact, self, other) {
        if (other.tag == 3) {
            if (this.canGather) {
                other.node.destroy();
                console.log("gather!");
                this.node.scaleX *= this.bigger_rate;
                this.node.scaleY *= this.bigger_rate;
                //this.getComponent(cc.PhysicsCircleCollider).radius *= Math.sqrt(this.bigger_rate);
                this.camera.zoomRatio *= this.camera_zoom_out_rate;
                // adjust UI apprearance with some ratio math
                this.game_manager.adjust_UI_apprearance(this.camera_zoom_out_rate);
                this.scatterNum--;
                this.anim.play("splitFace");
            }
        }
        else if (other.tag == 2) {
            console.log("test");
        }
    };
    Player.prototype.onEndContact = function (contact, self, other) {
        if (other.tag == 1) {
            this.isOnGround = false;
            var WorldManifold = contact.getWorldManifold().normal;
            console.log("end touch : " + WorldManifold);
            this.last_end_contact = WorldManifold;
        }
        else if (other.tag == 3) {
        }
    };
    Player.prototype.onPreSolve = function (contact, self, other) {
        if (other.tag == 1) {
            var WorldManifold = contact.getWorldManifold().normal;
            if (this.last_end_contact == WorldManifold && this.on_jumpable_floor(WorldManifold)) {
                this.isOnGround = true;
                this.last_end_contact = null;
            }
        }
    };
    Player.prototype.onKeyDown = function (event) {
        if (this.isDead)
            return;
        switch (event.keyCode) {
            case cc.macro.KEY.space:
                this.isJump = true;
                break;
            case cc.macro.KEY.z:
                this.triScatter = true;
                break;
            case cc.macro.KEY.x:
                this.triGather = true;
                console.log(this.isScatter);
                break;
        }
    };
    Player.prototype.onKeyUp = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.space:
                this.isJump = false;
                break;
            case cc.macro.KEY.z:
                this.triScatter = false;
                break;
            case cc.macro.KEY.x:
                this.triGather = false;
                this.canGather = false;
                break;
        }
    };
    Player.prototype.on_jumpable_floor = function (wm) {
        if (this.camera_script.rotate_angle >= 30) {
            if (wm.x >= Math.cos((270 + this.camera_script.rotate_angle - this.jumpable_angle) * 2 * Math.PI / 360)) {
                if ((wm.y <= Math.sin((270 + this.camera_script.rotate_angle + this.jumpable_angle) * 2 * Math.PI / 360))) {
                    console.log("on jumpable foor");
                    return true;
                }
            }
        }
        else if (this.camera_script.rotate_angle <= -30) {
            if (wm.x <= Math.cos((270 + this.camera_script.rotate_angle + this.jumpable_angle) * 2 * Math.PI / 360)) {
                if ((wm.y <= Math.sin((270 + this.camera_script.rotate_angle - this.jumpable_angle) * 2 * Math.PI / 360))) {
                    console.log("on jumpable foor");
                    return true;
                }
            }
        }
        else {
            if ((wm.x >= Math.cos((270 + this.camera_script.rotate_angle - this.jumpable_angle) * 2 * Math.PI / 360) &&
                wm.x <= Math.cos((270 + this.camera_script.rotate_angle + this.jumpable_angle) * 2 * Math.PI / 360))) {
                if (this.camera_script.rotate_angle >= 0) {
                    if ((wm.y <= Math.sin((270 + this.camera_script.rotate_angle + this.jumpable_angle) * 2 * Math.PI / 360))) {
                        console.log("on jumpable foor");
                        return true;
                    }
                }
                else {
                    if ((wm.y <= Math.sin((270 + this.camera_script.rotate_angle - this.jumpable_angle) * 2 * Math.PI / 360))) {
                        console.log("on jumpable foor");
                        return true;
                    }
                }
            }
        }
        return false;
    };
    Player.prototype.jump = function () {
        // TODO: aniamtion and sound effect
        if (!this.isDead && !this.anim.getAnimationState("hurtFace").isPlaying)
            this.anim.play("jumpFace");
        cc.audioEngine.playEffect(this.se_jump, false);
        this.isJump = false;
        this.isOnGround = false;
        // short delay before jump to match animation
        this.scheduleOnce(function () {
            this.getComponent(cc.RigidBody).applyForceToCenter(cc.v2(-this.jump_force * Math.pow(this.bigger_rate, 2 * (this.eatenNum - this.scatterNum)) * cc.director.getPhysicsManager().gravity.x, -this.jump_force * Math.pow(this.bigger_rate, 2 * (this.eatenNum - this.scatterNum)) * cc.director.getPhysicsManager().gravity.y), true);
        }, 0.05);
    };
    Player.prototype.scatter = function () {
        console.log("scatter!");
        var i = this.eatenNum - this.scatterNum;
        if (i > 0) {
            this.schedule(function () {
                var clone_cell = cc.instantiate(this.clone_cell);
                //this.cloneCellList[i] = clone_cell;
                clone_cell.parent = cc.director.getScene();
                clone_cell.setPosition(this.node.position);
                this.node.scaleX /= this.bigger_rate;
                this.node.scaleY /= this.bigger_rate;
                //this.getComponent(cc.PhysicsCircleCollider).radius /= Math.sqrt(this.bigger_rate);
                this.camera.zoomRatio /= this.camera_zoom_out_rate;
                this.anim.play("splitFace");
                this.scatterNum++;
                // adjust UI apprearance with some ratio math
                this.game_manager.adjust_UI_apprearance(1 / this.camera_zoom_out_rate);
            }, 0.1, i - 1, 0);
        }
        this.triScatter = false;
        this.isScatter = true;
    };
    Player.prototype.gather = function () {
        console.log("gather!");
        this.anim.play("splitFace");
        this.canGather = true;
    };
    Player.prototype.deadCtrl = function () {
        var _this = this;
        if (this.isDead && !this.isReborn) {
            console.log("reborning");
            this.isReborn = true;
            //play dead music 
            //play dead anim
            this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            this.scheduleOnce(function () { _this.game_manager.go_to_lose(); }, 2);
            // this.node.position = this.rebornPos; 
            this.bodyAnim.on('finished', this.changeDeadScene, this);
        }
    };
    Player.prototype.changeDeadScene = function () {
        this.anim.play("dieFace");
        // this.bodyAnim.stop();
        cc.log("change scene");
        // this.node.position = this.node.position;
        // this.scheduleOnce(function(){
        //         console.log("able to die again");
        //         this.isDead = false;
        //         this.isReborn = false;
        //     }, this.rebornTime);
    };
    __decorate([
        property
    ], Player.prototype, "bigger_rate", void 0);
    __decorate([
        property
    ], Player.prototype, "camera_zoom_out_rate", void 0);
    __decorate([
        property
    ], Player.prototype, "jump_force", void 0);
    __decorate([
        property(cc.Prefab)
    ], Player.prototype, "clone_cell", void 0);
    __decorate([
        property
    ], Player.prototype, "jumpable_angle", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Player.prototype, "bgm_stage1", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Player.prototype, "se_jump", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Player.prototype, "se_eatCell", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Player.prototype, "se_eatFruit", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Player.prototype, "se_hurt", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Player.prototype, "se_die", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Player.prototype, "skin1", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Player.prototype, "skin2", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Player.prototype, "skin3", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Player.prototype, "skin4", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Player.prototype, "skin5", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Player.prototype, "skin6", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Player.prototype, "skin7", void 0);
    Player = __decorate([
        ccclass
    ], Player);
    return Player;
}(cc.Component));
exports.default = Player;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Player.js.map
        