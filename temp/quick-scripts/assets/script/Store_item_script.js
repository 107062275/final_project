(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Store_item_script.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '6a05974+pRPKqj0+mjqle6x', 'Store_item_script', __filename);
// script/Store_item_script.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Store_item_script = /** @class */ (function (_super) {
    __extends(Store_item_script, _super);
    function Store_item_script() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.price = null;
        _this.total_manager = null;
        _this.store_manager = null;
        _this.item_cover = null;
        _this.issold = false;
        _this.ischoosed = false;
        _this.skin_number = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Store_item_script.prototype.onLoad = function () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
        this.store_manager = cc.find("Store_manager").getComponent("Store_manager");
        this.item_cover = this.node.parent.getChildByName("item cover");
    };
    Store_item_script.prototype.start = function () {
        this.price = +this.node.parent.getChildByName("item frame").getChildByName("price").getComponent(cc.Label).string;
        if (this.issold) {
            this.node.parent.getChildByName("item frame").getChildByName("price").active = false;
            this.node.parent.getChildByName("item frame").getChildByName("Sold").active = true;
        }
        if (this.ischoosed) {
            this.change_to_choosed_sprite();
        }
        console.log("price: " + this.price);
    };
    // update (dt) {}
    Store_item_script.prototype.choose = function (event, customEventData) {
        console.log("fruit_cur: " + this.total_manager.fruit);
        var skin_number = this.skin_number;
        if (!this.issold) {
            if (this.total_manager.fruit >= this.price) {
                //change price to sold
                this.node.parent.getChildByName("item frame").getChildByName("price").active = false;
                this.node.parent.getChildByName("item frame").getChildByName("Sold").active = true;
                // update fruit
                this.total_manager.fruit -= this.price;
                this.total_manager.update_to_firebase("fruit", this.total_manager.fruit);
                this.store_manager.player_fruit_label.string = "fruits : " + this.total_manager.fruit;
                this.update_skin();
                this.update_current_skin();
                console.log("fruit_cft: " + this.total_manager.fruit);
                console.log("price: " + this.price);
                this.ischoosed = true;
                this.issold = true;
                alert("You bought a new skin!");
            }
            else {
                alert("You don't have enough fruit!");
            }
        }
        else {
            if (!this.ischoosed) {
                this.update_current_skin();
                this.ischoosed = true;
                alert("change skin success!");
            }
        }
    };
    Store_item_script.prototype.update_skin = function () {
        this.total_manager.skin += Math.pow(2, (this.skin_number - 1));
        this.total_manager.update_to_firebase("skin", this.total_manager.skin);
    };
    Store_item_script.prototype.update_current_skin = function () {
        var previous_skin = this.store_manager.items[this.total_manager.current_skin - 1].getChildByName("Store_item_script").getComponent("Store_item_script");
        previous_skin.ischoosed = false;
        previous_skin.change_to_unchoosed_sprite();
        this.total_manager.current_skin = this.skin_number;
        this.total_manager.update_to_firebase("current_skin", this.skin_number);
        this.change_to_choosed_sprite();
    };
    Store_item_script.prototype.change_to_choosed_sprite = function () {
        console.log("change skin " + this.skin_number + " to choosed sprite!");
        //TODO : change to choosed sprite, maybe be darker 
        this.item_cover.opacity = 70;
    };
    Store_item_script.prototype.change_to_unchoosed_sprite = function () {
        console.log("change skin " + this.skin_number + " to unchoosed sprite!");
        //TODO : change to unchoosed sprite, maybe be lighter 
        this.item_cover.opacity = 0;
    };
    // use binary code to determine whether bought it or not 
    Store_item_script.prototype.isbought = function (skin_number) {
        var tmp_skin_number = this.total_manager.skin;
        for (var i = 1; i < skin_number; i++) {
            tmp_skin_number = Math.floor(tmp_skin_number / 2);
        }
        console.log("tmp_skin_number : " + tmp_skin_number);
        if (tmp_skin_number % 2) {
            return true;
        }
        else {
            return false;
        }
    };
    Store_item_script.prototype.set_skin_number = function (skin_number) {
        this.skin_number = skin_number;
    };
    Store_item_script = __decorate([
        ccclass
    ], Store_item_script);
    return Store_item_script;
}(cc.Component));
exports.default = Store_item_script;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Store_item_script.js.map
        