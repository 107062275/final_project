(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Store_manager.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'ab160Nfo+NF1Im6jv6ySIiQ', 'Store_manager', __filename);
// script/Store_manager.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Store_manager = /** @class */ (function (_super) {
    __extends(Store_manager, _super);
    function Store_manager() {
        // LIFE-CYCLE CALLBACKS:
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.player_fruit_label = null;
        _this.items = [];
        _this.total_manager = null;
        return _this;
        // update (dt) {}
    }
    Store_manager.prototype.onLoad = function () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
    };
    Store_manager.prototype.start = function () {
        this.player_fruit_label.string = "fruits : " + String(this.total_manager.fruit);
        this.init_items();
    };
    Store_manager.prototype.back_to_menu = function () {
        document.body.style.cursor = "auto";
        cc.director.loadScene("menu");
    };
    Store_manager.prototype.init_items = function () {
        for (var i = 0; i < this.items.length; i++) {
            var Store_item_script = this.items[i].getChildByName("Store_item_script").getComponent("Store_item_script");
            Store_item_script.set_skin_number(i + 1);
            if (this.total_manager.current_skin == i + 1) {
                Store_item_script.ischoosed = true;
            }
            if (Store_item_script.isbought(i + 1)) {
                Store_item_script.issold = true;
            }
        }
    };
    Store_manager.prototype.set_fruit = function () {
        var data = cc.find("fruit_set(debug)").getComponent(cc.EditBox).string;
        this.total_manager.update_to_firebase("fruit", data);
        alert("set success! plz logout and login");
    };
    Store_manager.prototype.clear_skin = function () {
        this.total_manager.update_to_firebase("skin", 1);
        this.total_manager.update_to_firebase("current_skin", 1);
        alert("clear success! plz logout and login");
    };
    __decorate([
        property({ type: cc.Label })
    ], Store_manager.prototype, "player_fruit_label", void 0);
    __decorate([
        property({ type: cc.Node })
    ], Store_manager.prototype, "items", void 0);
    Store_manager = __decorate([
        ccclass
    ], Store_manager);
    return Store_manager;
}(cc.Component));
exports.default = Store_manager;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Store_manager.js.map
        