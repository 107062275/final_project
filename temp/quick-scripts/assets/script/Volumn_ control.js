(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Volumn_ control.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '3a55fM2UA9Ah5mED0LZFSSx', 'Volumn_ control', __filename);
// script/Volumn_ control.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Volumn_control = /** @class */ (function (_super) {
    __extends(Volumn_control, _super);
    function Volumn_control() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BGM_bar = null;
        _this.Effect_bar = null;
        _this.BGM_point = null;
        _this.Effect_point = null;
        _this.total_manager = null;
        _this.canvas = null;
        _this.cur_BGM_volumn = null;
        _this.cur_Effect_volumn = null;
        _this.BGM_bar_width = null;
        _this.Effect_bar_width = null;
        _this.BGM_mouse_on = false;
        _this.Effect_mouse_on = false;
        return _this;
        // update (dt) {}
    }
    ;
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Volumn_control.prototype.start = function () {
        var _this = this;
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
        this.canvas = cc.find("Canvas");
        this.cur_BGM_volumn = this.total_manager.BGM_volumn;
        this.cur_Effect_volumn = this.total_manager.Effect_volumn;
        console.log(this.total_manager.Effect_volumn);
        this.BGM_bar_width = this.BGM_bar.width;
        this.Effect_bar_width = this.Effect_bar.width;
        this.BGM_point.setPosition(this.BGM_bar.position.x + (this.cur_BGM_volumn - 0.5) * this.BGM_bar_width, this.BGM_bar.position.y);
        this.Effect_point.setPosition(this.Effect_bar.position.x + (this.cur_Effect_volumn - 0.5) * this.Effect_bar_width, this.Effect_bar.position.y);
        this.BGM_point.on(cc.Node.EventType.MOUSE_DOWN, function (event) { this.BGM_mouse_on = true; }, this);
        this.node.on(cc.Node.EventType.MOUSE_MOVE, function (event) { _this.BGM_set_point_position_and_volumn(event); }, this);
        this.BGM_point.on(cc.Node.EventType.MOUSE_MOVE, function (event) { _this.BGM_set_point_position_and_volumn(event); }, this);
        this.BGM_point.on(cc.Node.EventType.MOUSE_UP, function (event) { this.BGM_mouse_on = false; }, this);
        this.node.on(cc.Node.EventType.MOUSE_UP, function (event) { this.BGM_mouse_on = false; }, this);
        this.canvas.on(cc.Node.EventType.MOUSE_UP, function (event) { this.BGM_mouse_on = false; }, this);
        this.Effect_point.on(cc.Node.EventType.MOUSE_DOWN, function (event) { this.Effect_mouse_on = true; }, this);
        this.node.on(cc.Node.EventType.MOUSE_MOVE, function (event) { _this.Effect_set_point_position_and_volumn(event); }, this);
        this.Effect_point.on(cc.Node.EventType.MOUSE_MOVE, function (event) { _this.Effect_set_point_position_and_volumn(event); }, this);
        this.Effect_point.on(cc.Node.EventType.MOUSE_UP, function (event) { this.Effect_mouse_on = false; }, this);
        this.node.on(cc.Node.EventType.MOUSE_UP, function (event) { this.Effect_mouse_on = false; }, this);
        this.canvas.on(cc.Node.EventType.MOUSE_UP, function (event) { this.Effect_mouse_on = false; }, this);
    };
    Volumn_control.prototype.BGM_set_point_position_and_volumn = function (event) {
        if (this.BGM_mouse_on) {
            var cur_x = event.getLocation().x;
            // Sets the position of the node in its parent's coordinates.
            this.BGM_point.x = cc.misc.clampf(cur_x - this.BGM_point.parent.parent.x - this.BGM_point.parent.x, -this.BGM_bar_width / 2 + this.BGM_bar.x, this.BGM_bar_width / 2 + this.BGM_bar.x);
            this.cur_BGM_volumn = (this.BGM_point.x - this.BGM_bar.x) / this.BGM_bar_width + 0.5;
            cc.audioEngine.setMusicVolume(this.cur_BGM_volumn);
            console.log("BGM volumn : " + cc.audioEngine.getMusicVolume());
            this.total_manager.BGM_volumn = this.cur_BGM_volumn;
        }
    };
    Volumn_control.prototype.Effect_set_point_position_and_volumn = function (event) {
        if (this.Effect_mouse_on) {
            var cur_x = event.getLocation().x;
            // Sets the position of the node in its parent's coordinates.
            this.Effect_point.x = cc.misc.clampf(cur_x - this.Effect_point.parent.parent.x - this.Effect_point.parent.x, -this.BGM_bar_width / 2 + this.Effect_bar.x, this.BGM_bar_width / 2 + this.Effect_bar.x);
            this.cur_Effect_volumn = (this.Effect_point.x - this.Effect_bar.x) / this.BGM_bar_width + 0.5;
            cc.audioEngine.setEffectsVolume(this.cur_Effect_volumn);
            console.log("Effect volumn : " + cc.audioEngine.getEffectsVolume());
            this.total_manager.Effect_volumn = this.cur_Effect_volumn;
            console.log(this.total_manager.Effect_volumn);
        }
    };
    __decorate([
        property(cc.Node)
    ], Volumn_control.prototype, "BGM_bar", void 0);
    __decorate([
        property(cc.Node)
    ], Volumn_control.prototype, "Effect_bar", void 0);
    __decorate([
        property(cc.Node)
    ], Volumn_control.prototype, "BGM_point", void 0);
    __decorate([
        property(cc.Node)
    ], Volumn_control.prototype, "Effect_point", void 0);
    Volumn_control = __decorate([
        ccclass
    ], Volumn_control);
    return Volumn_control;
}(cc.Component));
exports.default = Volumn_control;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Volumn_ control.js.map
        