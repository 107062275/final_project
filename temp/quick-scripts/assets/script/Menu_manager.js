(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Menu_manager.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'b499aPnq3RO84kkrhZCRuFj', 'Menu_manager', __filename);
// script/Menu_manager.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Menu_manager = /** @class */ (function (_super) {
    __extends(Menu_manager, _super);
    function Menu_manager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Total_manager = null;
        _this.body = null;
        _this.setting_box = null;
        _this.rule_box = null;
        _this.skin1 = null;
        _this.skin2 = null;
        _this.skin3 = null;
        _this.skin4 = null;
        _this.skin5 = null;
        _this.skin6 = null;
        _this.skin7 = null;
        _this.se_click = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Menu_manager.prototype.start = function () {
        this.Total_manager = cc.find("Total_manager").getComponent("Total_manager");
        this.body = cc.find("Canvas/Body");
        var current_skin = this.Total_manager.current_skin;
        cc.log("use skin " + current_skin);
        // TODO : init player sprite to the skin
        //        note : the order of skin is the order of items in (scence)"store"/"Store_manager"/"Items"
        switch (current_skin) {
            case 1:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin1;
                break;
            case 2:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin2;
                break;
            case 3:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin3;
                break;
            case 4:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin4;
                break;
            case 5:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin5;
                break;
            case 6:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin6;
                break;
            case 7:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin7;
                break;
        }
    };
    // update (dt) {}
    Menu_manager.prototype.turn_to_stage1 = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_stage1();
    };
    Menu_manager.prototype.turn_to_stage2 = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_stage2();
    };
    Menu_manager.prototype.turn_to_stage3 = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_stage3();
    };
    Menu_manager.prototype.turn_to_store = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_store();
    };
    Menu_manager.prototype.show_setting_box = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.setting_box.active = true;
    };
    Menu_manager.prototype.show_rule_box = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.rule_box.active = true;
    };
    Menu_manager.prototype.cancel_setting_box = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.setting_box.active = false;
        document.body.style.cursor = "auto";
    };
    Menu_manager.prototype.cancel_rule_box = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.rule_box.active = false;
        document.body.style.cursor = "auto";
    };
    Menu_manager.prototype.turn_to_test_stage = function () {
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_test_stage();
    };
    __decorate([
        property(cc.Node)
    ], Menu_manager.prototype, "setting_box", void 0);
    __decorate([
        property(cc.Node)
    ], Menu_manager.prototype, "rule_box", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Menu_manager.prototype, "skin1", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Menu_manager.prototype, "skin2", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Menu_manager.prototype, "skin3", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Menu_manager.prototype, "skin4", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Menu_manager.prototype, "skin5", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Menu_manager.prototype, "skin6", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Menu_manager.prototype, "skin7", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Menu_manager.prototype, "se_click", void 0);
    Menu_manager = __decorate([
        ccclass
    ], Menu_manager);
    return Menu_manager;
}(cc.Component));
exports.default = Menu_manager;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Menu_manager.js.map
        