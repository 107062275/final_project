// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Camera_control extends cc.Component {

    @property(cc.Node)
    player_node: cc.Node = null;

    @property
    max_rotate_angle: number = 60;

    // last percentage of rotation will be slow to stop
    @property
    slow_rotate_percentage: number = 1/5;

    private map:cc.Node = null;
    private canvas:cc.Node = null;
    private camera:cc.Node = null;

    rotate_angle : number = 0;

    private rightDown: boolean = false; // key for player to jump

    private leftDown: boolean = false; // key for player to jump

    // LIFE-CYCLE CALLBACKS:

     onLoad () {
        cc.director.getPhysicsManager().enabled = true;
     }

    start () {
        
        this.camera = this.node.parent;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    update (dt) {
            
        //camera follow player
        let player_pos = this.player_node.getPosition();
        this.camera.setPosition(player_pos);

        //rotate comara
        
            if(this.leftDown){
                
                if(this.camera.angle >=-this.max_rotate_angle){ 
                    if(this.camera.angle <=-this.max_rotate_angle * (1 - this.slow_rotate_percentage)){ //last part of rotation be slow
                        this.camera.angle -= (this.max_rotate_angle + this.camera.angle)/(this.max_rotate_angle * this.slow_rotate_percentage);
                    }
                    else
                        this.camera.angle -= 1;
                }
            }
            else if(this.rightDown){
                if(this.camera.angle <= this.max_rotate_angle){ 
                    if(this.camera.angle >= this.max_rotate_angle * (1 - this.slow_rotate_percentage)){ //last part of rotation be slow
                        this.camera.angle += (this.max_rotate_angle - this.camera.angle)/(this.max_rotate_angle * this.slow_rotate_percentage);
                    }
                    else
                        this.camera.angle += 1;
                }
            }
            this.rotate_angle = this.camera.angle;
        //rotate gravity
        cc.director.getPhysicsManager().gravity = cc.v2(-100 * Math.cos((this.camera.angle+90) * 2*Math.PI/360),
                                                        100 * Math.sin((this.camera.angle-90) * 2*Math.PI/360));
        
    }
    
    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.right:
                console.log(cc.macro.KEY.right)
                this.rightDown = true;
                break;
            case cc.macro.KEY.left:
                this.leftDown = true;
                break;
        }
    }
    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.right:
                this.rightDown = false;
                break;
            case cc.macro.KEY.left:
                this.leftDown = false;
                break;
        }
    }

}
