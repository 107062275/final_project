// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Cell extends cc.Component {

    onLoad () {

    }

    start () {
        let action;
        action = cc.repeatForever(cc.sequence(cc.moveBy(0.5, 0, 5), cc.moveBy(0.5, 0, -5)));
        this.node.runAction(action);
    
    }

    update (dt) {
        
    }

    onBeginContact( contact, self, other ){
        //if(other.tag == 0){ // touch by player
        //   this.eaten();
        //}
    }

    eaten(){
        //TODO : some sound effect or score calculate
        //console.log("be eaten! destroy!");
        //this.node.destroy();
    }
}
