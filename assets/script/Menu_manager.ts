// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu_manager extends cc.Component {

    private Total_manager : any = null;

    private body : any = null;

    @property(cc.Node)
    setting_box: cc.Node = null;

    @property(cc.Node)
    rule_box: cc.Node = null;

    @property(cc.SpriteFrame)
    skin1: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    skin2: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    skin3: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    skin4: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    skin5: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    skin6: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    skin7: cc.SpriteFrame = null;

    @property({type:cc.AudioClip})
    se_click: cc.AudioClip = null; 


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

        this.Total_manager = cc.find("Total_manager").getComponent("Total_manager");

    
        this.body = cc.find("Canvas/Body")

        var current_skin = this.Total_manager.current_skin;
        cc.log("use skin " + current_skin);

        // TODO : init player sprite to the skin
        //        note : the order of skin is the order of items in (scence)"store"/"Store_manager"/"Items"

        switch(current_skin){
            case 1:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin1;
                break;
            case 2:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin2;
                break;
            case 3:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin3;
                break;
            case 4:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin4;
                break;
            case 5:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin5;
                break;
            case 6:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin6;
                break;
            case 7:
                this.body.getComponent(cc.Sprite).spriteFrame = this.skin7;
                break;
        }

    }

    // update (dt) {}

    turn_to_stage1(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_stage1();
    }

    turn_to_stage2(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_stage2();
    }

    turn_to_stage3(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_stage3();
}

    turn_to_store(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_store();
    }

    show_setting_box(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.setting_box.active = true;
    }

    show_rule_box(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.rule_box.active = true;
    }
    cancel_setting_box(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.setting_box.active = false;
        document.body.style.cursor = "auto";
    }

    cancel_rule_box(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.rule_box.active = false;
        document.body.style.cursor = "auto";
    }




    turn_to_test_stage(){
        cc.audioEngine.playEffect(this.se_click, false);
        this.Total_manager.goto_test_stage();
    }

}
