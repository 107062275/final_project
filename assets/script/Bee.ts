

const {ccclass, property} = cc._decorator;

@ccclass
export default class Bee extends cc.Component {
    @property
    vertical: boolean = false;

    @property
    moveDistance: number = 100;


    private anim = null;

    onLoad () {
        this.anim = this.node.getComponent(cc.Animation);
    }

    start () {
        this.anim.play("bee");
        //TODO:add bee's se
        let action;
        if(this.vertical){
            action = cc.repeatForever(cc.sequence(cc.moveBy(2, 0, this.moveDistance), cc.delayTime(1), cc.moveBy(2, 0, -this.moveDistance),cc.delayTime(1)));
        }else{
            action = cc.repeatForever(cc.sequence(cc.moveBy(2, -this.moveDistance, 0), cc.delayTime(1), cc.flipX(true), cc.moveBy(2, this.moveDistance, 0),cc.delayTime(1), cc.flipX(false)));
        }
        this.node.runAction(action);
    }

    onBeginContact( contact, self, other ){
        if(other.tag == 9){
            this.anim.play("hurt");
            console.log('bee die');
            this.scheduleOnce(function(){
                this.node.destroy();
            }, 0.6);
        }
    }

}
