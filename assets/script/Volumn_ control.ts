// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Volumn_control extends cc.Component {

    @property(cc.Node)
    BGM_bar: cc.Node = null;

    @property(cc.Node)
    Effect_bar: cc.Node = null;

    @property(cc.Node)
    BGM_point: cc.Node = null;

    @property(cc.Node)
    Effect_point: cc.Node = null;



    private total_manager : any = null;;
    private canvas : any = null;
    private cur_BGM_volumn : number = null;
    private cur_Effect_volumn : number = null;
    private BGM_bar_width : number = null;
    private Effect_bar_width : number = null;
    private BGM_mouse_on : boolean = false;
    private Effect_mouse_on : boolean = false;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}


    start () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
        this.canvas = cc.find("Canvas");
        this.cur_BGM_volumn = this.total_manager.BGM_volumn;
        this.cur_Effect_volumn = this.total_manager.Effect_volumn;
        console.log(this.total_manager.Effect_volumn);

        this.BGM_bar_width = this.BGM_bar.width;
        this.Effect_bar_width = this.Effect_bar.width;

        this.BGM_point.setPosition(this.BGM_bar.position.x + (this.cur_BGM_volumn-0.5)*this.BGM_bar_width, this.BGM_bar.position.y);
        this.Effect_point.setPosition(this.Effect_bar.position.x + (this.cur_Effect_volumn-0.5)*this.Effect_bar_width, this.Effect_bar.position.y);
        


        this.BGM_point.on(cc.Node.EventType.MOUSE_DOWN, function(event){ this.BGM_mouse_on = true;}, this);

        this.node.on(cc.Node.EventType.MOUSE_MOVE, (event)=>{this.BGM_set_point_position_and_volumn(event)}, this);
        this.BGM_point.on(cc.Node.EventType.MOUSE_MOVE, (event)=>{this.BGM_set_point_position_and_volumn(event)}, this);
        
        this.BGM_point.on(cc.Node.EventType.MOUSE_UP, function(event){ this.BGM_mouse_on = false;}, this);
        this.node.on(cc.Node.EventType.MOUSE_UP, function(event){ this.BGM_mouse_on = false;}, this);
        this.canvas.on(cc.Node.EventType.MOUSE_UP, function(event){ this.BGM_mouse_on = false;}, this);


        this.Effect_point.on(cc.Node.EventType.MOUSE_DOWN, function(event){ this.Effect_mouse_on = true;}, this);

        this.node.on(cc.Node.EventType.MOUSE_MOVE, (event)=>{this.Effect_set_point_position_and_volumn(event)}, this);
        this.Effect_point.on(cc.Node.EventType.MOUSE_MOVE, (event)=>{this.Effect_set_point_position_and_volumn(event)}, this);
        
        this.Effect_point.on(cc.Node.EventType.MOUSE_UP, function(event){ this.Effect_mouse_on = false;}, this);
        this.node.on(cc.Node.EventType.MOUSE_UP, function(event){ this.Effect_mouse_on = false;}, this);
        this.canvas.on(cc.Node.EventType.MOUSE_UP, function(event){ this.Effect_mouse_on = false;}, this);

    

    }
    
    BGM_set_point_position_and_volumn(event){
            if(this.BGM_mouse_on){
                
            let cur_x = event.getLocation().x;
        
            // Sets the position of the node in its parent's coordinates.
            this.BGM_point.x = cc.misc.clampf(cur_x - this.BGM_point.parent.parent.x - this.BGM_point.parent.x,
                -this.BGM_bar_width/2  + this.BGM_bar.x,this.BGM_bar_width/2 + this.BGM_bar.x);

            this.cur_BGM_volumn = (this.BGM_point.x - this.BGM_bar.x)/this.BGM_bar_width + 0.5;
            cc.audioEngine.setMusicVolume(this.cur_BGM_volumn);
            console.log("BGM volumn : " + cc.audioEngine.getMusicVolume());

            this.total_manager.BGM_volumn = this.cur_BGM_volumn;
            }
    }

    Effect_set_point_position_and_volumn(event){
        if(this.Effect_mouse_on){
            
        let cur_x = event.getLocation().x;
    
        // Sets the position of the node in its parent's coordinates.
        this.Effect_point.x = cc.misc.clampf(cur_x - this.Effect_point.parent.parent.x - this.Effect_point.parent.x,
            -this.BGM_bar_width/2  + this.Effect_bar.x,this.BGM_bar_width/2 + this.Effect_bar.x);

        this.cur_Effect_volumn = (this.Effect_point.x - this.Effect_bar.x)/this.BGM_bar_width + 0.5;
            cc.audioEngine.setEffectsVolume(this.cur_Effect_volumn);
        console.log("Effect volumn : " + cc.audioEngine.getEffectsVolume());

        this.total_manager.Effect_volumn = this.cur_Effect_volumn;
        console.log(this.total_manager.Effect_volumn);
        }
    }



    // update (dt) {}
}
