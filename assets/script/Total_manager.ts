// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Total_manager extends cc.Component {

    @property({type:cc.AudioClip})
    bgm_menu: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    bgm_game: cc.AudioClip = null;


    //public bgm_menu_State = null;



    stage : number = 1;
    
    cur_life : number = 10;

    account : string = null;

    fruit : number = null;
    cell : number = null;
    skin : number = null;
    current_skin : number = null;
    stage1_fruit : number = null;
    stage2_fruit : number = null;
    stage3_fruit : number = null;
    BGM_volumn : number = 0.5;
    Effect_volumn : number = 0.5;

    start () {
    
        cc.game.addPersistRootNode(this.node);
        cc.audioEngine.playMusic(this.bgm_menu, true);       

    }

    onLoad(){
        
    }

    goto_stage1(){   
        this.stage = 1;
        document.body.style.cursor = "auto";
        cc.director.loadScene("stage1",()=>{cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(this.bgm_game, true); });
        
    }

    goto_stage2(){ 
        this.stage = 2;
        document.body.style.cursor = "auto";
        cc.director.loadScene("stage2",()=>{cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(this.bgm_game, true); });
    }

    goto_stage3(){ 
        this.stage = 3;
        document.body.style.cursor = "auto";
        cc.director.loadScene("stage3",()=>{cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(this.bgm_game, true); });
    }

    goto_store(){
        document.body.style.cursor = "auto";
        cc.director.loadScene("store");
    }

    goto_menu(){
        if(cc.audioEngine.isMusicPlaying() ){
            cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(this.bgm_menu, true); 
        }
        document.body.style.cursor = "auto";
        cc.director.loadScene("menu");
    }

    goto_win_scene(){
        cc.director.loadScene("win");
    }

    goto_lose_scene(){
        cc.director.loadScene("lose");
    }
    
    update_to_firebase(operation:string, data:any){

        var postsRef = firebase.database().ref("account/" + this.account);   

        if(operation == "fruit"){
            postsRef.update({
                fruit : data
            });
        }
        else if(operation == "skin"){
            postsRef.update({
                skin : data
            });
        }
        else if(operation == "current_skin"){
            postsRef.update({
                current_skin : data
            });
        }
        else if(operation == "stage1_fruit"){
            postsRef.update({
                stage1_fruit : data
            });
        }
        else if(operation == "stage2_fruit"){
            postsRef.update({
                stage2_fruit : data
            });
        }
        else if(operation == "stage3_fruit"){
            postsRef.update({
                stage3_fruit : data
            });
        }
    }


    goto_test_stage(){
        document.body.style.cursor = "auto";
        cc.director.loadScene("test_scence");
    }

}
