"use strict";
cc._RF.push(module, 'f8329bspI5ORr7thMP/jEAx', 'Clonecell');
// script/Clonecell.ts

Object.defineProperty(exports, "__esModule", { value: true });
var Player_1 = require("./Player");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Clonecell = /** @class */ (function (_super) {
    __extends(Clonecell, _super);
    function Clonecell() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.player = null;
        _this.canGather = false;
        _this.anim = null;
        _this.bodyAnim = null;
        return _this;
    }
    Clonecell.prototype.onLoad = function () {
        this.anim = this.node.getChildByName("mini face").getComponent(cc.Animation);
        this.bodyAnim = this.node.getComponent(cc.Animation);
    };
    Clonecell.prototype.start = function () {
        this.player = cc.find("player");
        this.schedule(function () {
            this.getComponent(cc.RigidBody).linearVelocity = this.player.getComponent(cc.RigidBody).linearVelocity;
        }, 0.1);
    };
    Clonecell.prototype.onBeginContact = function (contact, self, other) {
        if (other.tag == 5) {
            console.log("clonecell touch enemy!");
            this.player.getComponent(Player_1.default).isHurt = true;
            this.player.getComponent(Player_1.default).eatenNum--;
            this.player.getComponent(Player_1.default).scatterNum--;
            console.log(this.player.getComponent(Player_1.default).eatenNum);
            //this.player.anim.play("hurtFace");
            if (!this.bodyAnim.getAnimationState("die").isPlaying) {
                this.anim.play("dieFace");
                this.bodyAnim.play("die");
            }
            // this.scheduleOnce(this.dead(), 0.8);
            this.bodyAnim.on('finished', this.dead, this);
            // this.node.destroy();
            // update UI cell label
            this.player.getComponent(Player_1.default).game_manager.update_UI_cell_lable(this.player.getComponent(Player_1.default).eatenNum);
        }
    };
    Clonecell.prototype.dead = function () {
        cc.log("cell dead");
        this.node.destroy();
    };
    Clonecell = __decorate([
        ccclass
    ], Clonecell);
    return Clonecell;
}(cc.Component));
exports.default = Clonecell;

cc._RF.pop();