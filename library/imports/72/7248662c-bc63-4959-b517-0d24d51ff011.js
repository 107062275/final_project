"use strict";
cc._RF.push(module, '72486YsvGNJWbUXDSTVH/AR', 'Total_manager');
// script/Total_manager.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Total_manager = /** @class */ (function (_super) {
    __extends(Total_manager, _super);
    function Total_manager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.bgm_menu = null;
        _this.bgm_game = null;
        //public bgm_menu_State = null;
        _this.stage = 1;
        _this.cur_life = 10;
        _this.account = null;
        _this.fruit = null;
        _this.cell = null;
        _this.skin = null;
        _this.current_skin = null;
        _this.stage1_fruit = null;
        _this.stage2_fruit = null;
        _this.stage3_fruit = null;
        _this.BGM_volumn = 0.5;
        _this.Effect_volumn = 0.5;
        return _this;
    }
    Total_manager.prototype.start = function () {
        cc.game.addPersistRootNode(this.node);
        cc.audioEngine.playMusic(this.bgm_menu, true);
    };
    Total_manager.prototype.onLoad = function () {
    };
    Total_manager.prototype.goto_stage1 = function () {
        var _this = this;
        this.stage = 1;
        document.body.style.cursor = "auto";
        cc.director.loadScene("stage1", function () {
            cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(_this.bgm_game, true);
        });
    };
    Total_manager.prototype.goto_stage2 = function () {
        var _this = this;
        this.stage = 2;
        document.body.style.cursor = "auto";
        cc.director.loadScene("stage2", function () {
            cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(_this.bgm_game, true);
        });
    };
    Total_manager.prototype.goto_stage3 = function () {
        var _this = this;
        this.stage = 3;
        document.body.style.cursor = "auto";
        cc.director.loadScene("stage3", function () {
            cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(_this.bgm_game, true);
        });
    };
    Total_manager.prototype.goto_store = function () {
        document.body.style.cursor = "auto";
        cc.director.loadScene("store");
    };
    Total_manager.prototype.goto_menu = function () {
        if (cc.audioEngine.isMusicPlaying()) {
            cc.audioEngine.stopAll();
            cc.audioEngine.playMusic(this.bgm_menu, true);
        }
        document.body.style.cursor = "auto";
        cc.director.loadScene("menu");
    };
    Total_manager.prototype.goto_win_scene = function () {
        cc.director.loadScene("win");
    };
    Total_manager.prototype.goto_lose_scene = function () {
        cc.director.loadScene("lose");
    };
    Total_manager.prototype.update_to_firebase = function (operation, data) {
        var postsRef = firebase.database().ref("account/" + this.account);
        if (operation == "fruit") {
            postsRef.update({
                fruit: data
            });
        }
        else if (operation == "skin") {
            postsRef.update({
                skin: data
            });
        }
        else if (operation == "current_skin") {
            postsRef.update({
                current_skin: data
            });
        }
        else if (operation == "stage1_fruit") {
            postsRef.update({
                stage1_fruit: data
            });
        }
        else if (operation == "stage2_fruit") {
            postsRef.update({
                stage2_fruit: data
            });
        }
        else if (operation == "stage3_fruit") {
            postsRef.update({
                stage3_fruit: data
            });
        }
    };
    Total_manager.prototype.goto_test_stage = function () {
        document.body.style.cursor = "auto";
        cc.director.loadScene("test_scence");
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], Total_manager.prototype, "bgm_menu", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Total_manager.prototype, "bgm_game", void 0);
    Total_manager = __decorate([
        ccclass
    ], Total_manager);
    return Total_manager;
}(cc.Component));
exports.default = Total_manager;

cc._RF.pop();